%!TEX encoding = UTF-8 Unicode
\documentclass[twocolumn, 10pt]{article}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[margin=2cm, top=32mm, columnsep=20pt]{geometry}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{bbm}
\usepackage{comment}

\usepackage[colorlinks=true,linkcolor=red,urlcolor=blue,filecolor=green]{hyperref}


\title{Kernel methods for machine learning \\ Classification d'ADN \\ \begin{small}public score : 0.670 - private score : 0.644 \end{small}}
\date{\today}
\author{Mathieu RANDON}

\begin{document}
\maketitle
\begin{abstract}
Cette étude porte sur la classification de séquences d'ADN avec des méthodes d'apprentissage machine par noyau - kernel machines. Plusieurs noyaux utilisant différentes représentations des séquences ADN sont comparés par validation croisée. L’inférence du modèle, d'abord réalisée par regression ridge à noyaux (KRR), a ensuite été étendue aux machines à vecteur de support (SVM). La meilleure justesse de classification (0.686 en moyenne) est obtenue par KRR en additionnant les meilleures noyaux spectres. Les données et codes source de cette étude sont téléchargeable via le lien en bas de page\footnote{\url{https://gitlab.utc.fr/randonma/kmml2021}}.
\end{abstract}

\section{Introduction}
\label{sec:introduction}
Les organismes biologiques sont constitués de protéines, une séquence d'acides-aminés encodée par les gènes de l'ADN. La production de ces protéines est régulée par d'autres protéines appelées facteurs de transcription (transcription factor - TF), qui agissent ainsi sur la division, la croissance, ou la mort des cellules de l'organisme. Pour cela, le TF doit se fixer à une séquence d'ADN - DNA binding-domain (DBD), pour amplifier ou inhiber la transcription d'un gène en protéine. Peut-on classifier par apprentissage automatique les séquences d'ADN auxquels un TF va pouvoir se lier ? Plus particulièrement, cette étude expérimentera les méthodes d'apprentissage par noyaux - kernel machines.

Le problème de classification est posé en partie~\ref{sec:probleme}. La méthode d'apprentissage par noyaux est décrite en partie~\ref{sec:methode}. Les résultats obtenus sont discutés en partie~\ref{sec:resultats}. La partie~\ref{sec:conclusion} conclue ce rapport avec les enseignements clefs et les perspectives.

\section{Problème}
\label{sec:probleme}

\subsection{Classification}
Nous souhaitons apprendre un modèle $h_\alpha$ paramétré par $\alpha$ permettant de prédire si un DBD appartient ($y_i=1$) ou non ($y_i=-1$) à une séquence d’ADN $x_i \in \mathcal{X}$. L'étude s'appuie sur trois bases de données $\mathcal{D}^b = \{X^b, Y^b, X^{b\star} \}, {b=\{0,1,2\}}$ correspondant à trois TFs différents. Pour l’apprentissage du modèle, chaque base contient $n=2000$ séquences d’ADN $X^b \in \mathcal{X}^n$ étiquetées par classes d’appartenance $Y^b \in \{-1,1\}^n$. Le modèle appris doit ensuite prédire la classe d’appartenance $Y^{b\star}$ sur $1000$ séquences d'ADN non-étiquetées $X^{b\star}$. 

Habituellement en apprentissage automatique, les classifications sont réalisées avec des données vectorielles $\{ X^b, X^{b\star} \}$, qu'il s'agisse d'une mesure d'un capteur, ou d'une image numérique. La représentation d'une séquence d'ADN est moins évidente, plusieurs possibilités existes. 

\subsection{Nature des données}
\label{subsec:pretraitements} 

Une séquence d'ADN est généralement représentée par une chaîne de caractères, ou chaque lettre correspond à un nucléotide A, T, G ou C, éléments de l'ADN. Par exemple $x_{a,i} = ATG \dots CGA$. 

Alternativement, nous avons proposé de convertir l'ADN en protéines. Pour le même exemple, on obtient $x_{p,i} = M \dots R$. Cette représentation permet de réduire les $36$ combinaisons de trois nucléotides à $20$ possibilités d'acides aminés. Ne sachant pas à quel nucléotide commence la traduction, les trois séquences de protéines possibles $\{X_{p_0}, X_{p_1}, X_{p_2}\}$ ont été calculées dans un vecteur en décalant la traduction d'un nucléotide. La représentation en protéines va permettre de comparer des séquences de plus grande taille que sous forme d'ADN.

Enfin, chaque séquence d'ADN est aussi représentée par un vecteur $x_{c,i} \in \mathbb{R}^m$ correspondant aux probabilités que la séquence appartienne à l'un des $m=100$ groupes (ou cluster) prédéfinis. Ce vecteur est préalablement calculé par l'algorithme des K plus proches voisins. Cette transformation représente une séquence d'ADN sous forme vectorielle au prix d'une possible perte d'information.

Dans la section~\ref{sec:methode}, nous allons voir comment l'apprentissage par noyaux permet de réaliser la classification avec cette diversité de représentations d'ADN.

\section{Apprentissage par noyaux}
\label{sec:methode}

L’apprentissage par noyaux permet d'effectuer des prédictions avec la comparaison entre chaque séquence d'ADN, plutôt qu'avec l'ADN seul. La fonction de comparaison s'appelle un noyau $K \in \mathcal{H}$. Le choix judicieux du noyau (section~\ref{subsec:kernels}) permet de réaliser l’inférence du modèle avec des hypothèses linéaires (section~\ref{subsec:inference}). La comparaison des performances de prédiction en fonction du noyau et la régulation des méthodes d'inférence ont été réalisées par validation croisée des modèles (section~\ref{subsec:validation}).

\subsection{Noyaux}
\label{subsec:kernels}

Cette étude compare les résultats obtenus avec 3 catégories de noyaux (linéaire, gaussien, spectre), puis l’addition les noyaux entre eux. 

Le noyau linaire $K_l$ impose à chaque point d'influer équitablement l’ensemble des prédictions. Il s’agit de la matrice de covariance des données classiquement utilisée dans la régression linéaire. 

\begin{equation*}
K_l(x_{c,i}, x_{c,j}) = x_{c,i} \cdot x_{c,j}^T
\end{equation*}

Le noyau gaussien $K_{g,\sigma}$ définit une influence locale des points sur les prédictions. L’étendu de l’influence d’un point est paramétré par le paramètre $\sigma$. Il définit des fonctions non-linéaires infiniment dérivables. 

\begin{equation*}
K_{g,\sigma}(x_{c,i}, x_{c,j}) = e^{- \frac{||x_{c,i} - x_{c,j}||_2^2}{2\sigma^2}}
\end{equation*}

Le noyau spectre $K_{s,k}$ \cite{leslie_2002} est utilisé pour des données séquentielles. Il décompose la séquence en mots $u$ de taille $k$, et énumère le nombre d’occurrences de $u$ dans un dictionnaire des mots possibles $\mathcal{A}^k$. Cette énumération définit un spectre $\Phi_k(x_{a,i}) \in \mathbb{R}^k$. Le noyau spectre n’est autre que le noyau linéaire des spectres des séquences d’ADN. La normalisation des spectres améliore l’estimation \cite{graf_2003}. 

\begin{equation*}
K_{s,k}(x_{a,i}, x_{a,j}) = \frac{\Phi_k(x_{a,i})}{||\Phi_k(x_{a,i})||_2} \cdot \left({\frac{\Phi_k(x_{a,j})}{||\Phi_k(x_{a,j})||_2}}\right)^T
\end{equation*}

Le noyau somme $K_{a}$ additionne plusieurs noyaux entre eux. Il permet de fusionner l’influence de noyaux sur les prédictions. Par exemple, il a été utilisé pour fusionner les noyaux spectres des différentes possibilités de séquences de protéines~(voir section \ref{subsec:pretraitements}).

\begin{equation*}
K_{p,k} = \sum_{i=0}^2 \sum_{j=i}^2K_{s,k}(X_{p_i}, X_{p_j})
\end{equation*}

$K_{p,k}$ peut ensuite être additionné avec d'autre noyaux. Dans le reste de l'étude, on définit $K_{a}$ ainsi.

\begin{equation*}
K_a = K_{s,6} + K_{s,9} + K_{p,3} + K_{p,4}
\end{equation*}


\subsection{Inférence}
\label{subsec:inference}

En première approche, on fait l’hypothèse que le modèle est une combinaison linéaire d'un noyau $K$ paramétré par $\alpha$.
\begin{equation*}
h_\alpha (X^\star) = K(X^\star,X) \cdot \alpha
\end{equation*}
Ce qui permet d’apprendre $\alpha$ par regression ridge à noyaux (KRR) avec un facteur de régularisation $\lambda$. 
\begin{equation*}
\alpha = (K(X,X) + \lambda n I)^{-1} \cdot y
\end{equation*}
Les prédictions de ce modèle sont ensuite attribuées à la classe la plus proche de la classification. 

En deuxième approche, la classification est directement entrainée avec le signe des prédictions du modèle $sign(h_\alpha (X^\star))$. 
Pour cela, des machines à vecteur de support (SVM) réalisent l'inférence de $\alpha$ par la résolution d'un problème d'optimisation convexe.
\begin{equation*}
\underset{0\leq \mu \leq 1/n}{\text{max}} \mu^T1 - \frac{1}{4\lambda} \mu^T \text{diag}(Y)K\text{diag}(Y)\mu \; ; \; \alpha = \frac{\text{diag}(Y)\mu}{2\lambda}
\end{equation*}

Dans ces deux approches, le choix des facteurs de régularisation $\lambda$ est très important pour éviter le sur-apprentissage. 

\subsection{Validation croisée}
\label{subsec:validation}

Le choix des noyaux $\{K_l, K_g, K_s, K_p, K_a\}$, de leur paramètres $\{\sigma, k\}$, et des facteurs de régularisation $\lambda$ a été réalisé de façon à maximiser la justesse de classification $J$.
\begin{equation*}
J=\frac{\sum_{i=0}^n relu(Y\cdot h_\alpha (X))}{n}
\end{equation*}

Pour limiter le risque de sur-apprentissage, la justesse été évaluée par validation croisée : chaque base de données $\mathcal{D}^b$ est divisée en cinq jeux $\mathcal{D}^{b,g} , g=\{1,\dots,5\}$. Quatre jeux ont servi à l'entraînement et le dernier à l'évaluation de $J$. Nous avons utilisé la moyenne des justesses $\bar J$ sur chaque base pour comparer nos modèles.
%\footnote{C’est une erreur de méthode car ce critère ne prend pas en compte la dispersion des précisions, et donc le risque d’erreur sur un nouveau jeux de données. A l’avenir il sera plus judicieux de considérer une métrique plus robuste comme la précision minimale atteinte sur la validation croisée.}. 

\section{Résultats}
\label{sec:resultats}

Les meilleures prédictions ($\bar J = 0.686$) ont été obtenues par KRR avec le noyau somme $K_a$, qui fusionne l'information des quatres meilleurs noyaux spectre (voir figure~\ref{fig:score_by_kernel}). Les noyaux spectre d'ADN $K_{s,6}$ et $K_{s,9}$ sont meilleurs que les noyaux spectre de protéines $K_{p,3}$ et $K_{p,4}$ ; même si ces dernier ont l'avantage de réduire la dispersion des prédictions sur $D^1$. Ces quatre noyaux spectre sont meilleurs que le noyaux gaussien $K_{g,0.1}$ et linéaire $K_l$. Ce résultat montre l'importance du choix du noyau en fonction du problème d'apprentissage. Cependant, la meilleur justesse moyenne atteinte ($\bar J = 0.686$) reste faible pour un problème de classification. L'augmentation de la taille des spectres ne semble pas améliorer systématiquement cette justesse : $K_{s,6}$ est plus juste que $K_{s,9}$. De même $K_{p,3}$ est plus juste que $K_{p,4}$. Le lecteur remarquera aussi que les prédictions sont systématiquement meilleures pour le TF de $D^2$. Il s'en suit une forte variance des prédictions pour tous les noyaux. 
\begin{comment}
Pour améliorer ce score il faudrait considérer d'autres structures de noyau plus proche du problème : en admétant par exemple des erreurs probables de séquençage dans les données d'entraînement.  
\end{comment}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/score_by_kernels.pdf}
\caption{\label{fig:score_by_kernel} Justesses de la KRR pour 7 noyaux. Chaque point représente la justesse évaluée sur un échantillion de validation. La couleur des points correspond au TF de chaque base $\mathcal{D}^{\{0,1,2\}}$.}
\end{center}
\end{figure}

Sur le plan méthodologique cette étude a permis de mettre en avant l'importance d'optimiser systématiquement le facteur de régulation $\lambda$. Par exemple pour le noyau $K_a$, les meilleures justesses moyennes sont obtenues quand $\lambda = 0.001 \text{ sur } \mathcal{D}^0$, $\lambda = 0.003 \text{ sur } \mathcal{D}^1$, et $\lambda = 0.0003 \text{ sur } \mathcal{D}^2$ (voir figure~\ref{fig:score_by_lambda}).

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/score_by_lambda.pdf}
\caption{\label{fig:score_by_lambda} Justesse de la KRR du noyau $K_a$ pour différents facteur de régulation $\lambda$. La couleur des boîtes à moustaches correspond à chaque base $\mathcal{D}^{\{0,1,2\}}$.}
\end{center}
\end{figure}

Après optimisation de $\lambda$, le choix optimal des paramètres d'une famille de noyaux améliore la justesse atteignable. Par exemple, le noyau gaussien $K_{g,\sigma}$ obtient les meilleures $\bar J$ en KRR avec $\sigma =  0.1$  (voir figure~\ref{fig:gaussian_kernel_opti}).  

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/gaussian_kernel_sigma_opti.pdf}
\caption{\label{fig:gaussian_kernel_opti} Justesses de la KRR obtenue avec les noyaux gaussien $K_{g,\sigma}$ en fonction du paramètre $\sigma$.}
\end{center}
\end{figure}

Enfin, après selection du meilleur noyaux $K_a$ grâce à la rapidité de calcul en KKR, il est possible de comparer les méthodes d'inférence. L'apprentissage en SVM pour $K_a$ a été réalisé en suivant la même méthode d'optimision de $\lambda$.  La KRR est resté légèrement meilleure que la SVM (voir figure~\ref{fig:score_by_method}). Cela peut s'expliquer par la difficulté de réaliser la classification sur ce dataset (meilleure justesse obtenue de 0.686). La KRR semble plus robuste sur cette classification difficile. 

\begin{comment}
Ce résultat pourrait changer si nous trouvons un noyau plus adapté au problème. Il est aussi envisageable qu'un ajustement plus fin du facteur de régulation lambda améliore la justesse des SVMs. 
\end{comment}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/score_by_method.pdf}
\caption{\label{fig:score_by_method} Comparaison de la justesse de KRR et des SVM pour le meilleur noyau $K_{a}$}
\end{center}
\end{figure}


\section{Conclusion}
\label{sec:conclusion}

Cette étude a permis d'appliquer les méthodes d'apprentissage par noyau à un problème de classification d’ADN dont la représentation est complexe. 
Les méthodes par noyau permettent d’apprendre un modèle à partir d'une comparaison entre les données -le noyau- plutôt qu’à partir de la donnée seule. Cette représentation à l’avantage de permettre la fusion de plusieurs sources d’information en additionnant simplement les noyaux. L’addition des noyaux spectres de l’ADN et des protéines ont permis d’obtenir les meilleures prédictions ($\bar J = 0.658$). 
L’autre intérêt est l’extension des méthodes d’apprentissage habituellement réservées aux modèles linéaires, à l’apprentissage de modèles non-linéaires. Il suffit de choisir un noyau adapté au problème. Ce choix du noyau n'est pas trivial. Plusieurs familles de noyau ont été comparées après avoir optimisé leurs paramètres, comme $\sigma$ pour le noyau gaussien. 
Le sur-apprentissage est contrôlé par le choix optimal du facteur de régulation $\lambda$ de la méthode d'inférence.
Cette démarche nous a permis d'améliorer le modèle jusqu’à une justesse moyenne de classification de 0.686, qui reste cependant faible pour une classification. 



Pour améliorer cette performance, il semble prometteur d’étendre l'étude a des noyaux plus élaborés comme le \textit{mismatch spectrum kernel} \cite{leslie_2002_2} qui pourrait être plus robuste à d’éventuelles erreurs de séquençage de l'ADN ; ou un \textit{string alignment kernel} \cite{vert_2004} qui permettrait de comparer les séquences d’ADN selon un alignement proche. Pour aller plus loin, nous pouvons étendre la modélisation dans une représentation bayésienne, approche est très rependue avec les \textit{gaussian process} \cite{rasmussen_2005}. Dans cette approche, le facteur de régulation devient plus explicite : il définit l'à-priori sur le résidu gaussien du modèle. Les paramètres du noyau peuvent aussi être optimisés par maximisation de la vraisemblance des données par rapport au modèle.


\bibliographystyle{plain}
\bibliography{Biblio}

\begin{comment}
\appendix

\section{De l'ADN à la protéine}
\label{ann:traduction}
Lors de la traduction de l'ADN en protéine, chaque codons - suite de 3 nucléotides - est converti en acide aminé, élément d'une protéine. 
\end{comment}

\end{document}